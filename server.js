const io = require("socket.io")();
var fs = require("fs");

const port = 8000;
io.listen(port);
console.log("listening on port ", port);

let list = getList();
io.emit("populateList", list);

function getList() {
  let data = fs.readFileSync("users.json");
  let jsonData = JSON.parse(data);

  jsonData.sort((a, b) => {
    if (a.name > b.name) return 1;
    if (a.name < b.name) return -1;
    return 0;
  });
  return jsonData;
}

function updateList(message) {
  const index = list.findIndex(item => item.id === message.id);
  list[index] = message;
}

function prepareForSaving() {
  let listCopy = [...list];
  listCopy.sort((a, b) => a.id - b.id);
  jsonData = JSON.stringify(listCopy, null, 2);

  return jsonData;
}

io.on("connection", client => {
  io.emit("populateList", list);

  client.on("updateItemOnServer", message => {
    updateList(message);

    io.emit("updateItemOnClient", list);

    const jsonData = prepareForSaving();

    fs.writeFile("users.json", jsonData, err => {
      if (err) throw err;
    });
  });
});
